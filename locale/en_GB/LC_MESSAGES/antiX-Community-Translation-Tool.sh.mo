��          �      <      �  !)  �  .   �*  .   +  C   1+  �   u+  #   ,  D   5,  Y   z,  M   �,  z   "-  C   �-  >   �-  
    .  _   +.     �.     �.     �.  _  �.  ;&  0     LV  $   lV  :   �V  �   �V      aW  :   �W  D   �W  K   X  �   NX  8   �X  <   Y     MY  ]   ZY     �Y     �Y     �Y                                      	   
                                                              aCMTT antiX Community Multilanguage Translation Tool, Ver. $version

  Programmaufruf: aCMTT [Optionen] \"<Textzeile>\"

  (-Kurzform  --Langform  <Argumente>  Beschreibung)  Es darf immer nur wahl-
  weise entweder die Kurz- oder die Langform angegeben werden. Argumente <...>
  gelten für beide Formen.

  -a --allow-cookies   Erlaubt dem Überstzungsdienstleister die Verwendung von 
                       Cookies und Sitzungscookies. Die Cookiedatei wird aus
                       Datenschutzgründen von aCMTT automatisch gelöscht, sobald
                       sie älter als 1 Tag ist.

  -c --disable-check-connection  Diese Option deaktiviert die vorherige Überprü-
                       fung der Verbindung zum Übersetzungsdienstleister. Damit
                       können lange Wartezeiten auf ausbleibende Serverantworten
                       vermieden werden.

  -d --display-settings  Zeigt den aktuellen Inhalt der Standardkonfigurations-
                       datei an und verläßt das Programm.
  -d --display-settings <Konfigurationsdatei>  Zeigt den aktuellen Inhalt der
                       mit <Konfigurationsdatei> angegebene Datei an und verläßt
                       das Programm.

  -e --email-address   Aktiviert für diese Abfrage die Übertragung einer in der
                       Standardkonfigurationsdatei hinterlegten Emailadresse.
                       Dazu muß die Option -f zusätzlich angegeben sein. Die in
                       der Konfigurationsdatei definierte Übertragungseinstell-
                       ung wird dabei übergangen.
  -e --email-address <emailadresse>  Übermittelt bei dieser Abfrage die mit
                       <emailadresse> angegebene Anschrift an den Übersetzungs-
                       dienstleister (Zur Nutzung des erhöhten Kontingents)

  -f --from-settings   Läd alle in der Standard-Konfigurationsdatei 
                       ~/.config/aCMTT/aCMTT-settings enthaltenen Einstellungen.
  -f --from-settings <settingsfile>  läd aus der mit “<settingsfile>” angegebe-
                       nen Konfigurationsdatei alle darin vorhandenen Einstel-
                       lungen. Alle mit -f eingelesenen Festlegungen können
                       durch Angabe weiterer Optionen ergänzt oder abgeändert
                       werden, ohne die Konfigurationsdatei dabei
                       zu beeinflussen.

  -g --gui             Aktiviert die graphische Benutzeroberfläche (GUI) des
                       Programms. (In Version 0.18 noch nicht verfügbar)

  -h --help            Kurzhilfe zum Programm

  -i --ip-address      Aktiviert die zusätzliche Übertragung der der aktuellen
                       öffentlichen IP Adresse an den Übersetzungsdienstleister
                       (Vom Dienstleister bei starker Nutzung empfohlen)

  -k --api-key         Aktiviert die Übermittelung des in einer Konfigurations-
                       datei hinterlegten Schlüssels, sofern zusätzlich die
                       Option -f angegeben wird. Die in der Konfigurationsdatei
                       definierte Übertragungseinstellung wird dabei übergangen.
  -k --api-key <api-key>  Übermittelt den mit <api-key> angegebenen Schlüssel.
                       Dieser Schlüssel wird nur für die Nutzung einer privaten
                       Übersetzungsbibliothek benötigt und kann bei Bedarf unter
                       https://mymemory.translated.net/doc/keygen.php angefor-
                       dert werden.

  -l --line \"Textzeile\"  Obligatorische Angabe der zu übersetzenden Textzeile.
                       Der Text muß in doppelte Anführungszeichen eingschlossen
                       werden und darf selbst keine solchen Anführungszeichen
                       enthalten. Anstelledessen können “ bzw. „ und ” verwendet
                       werden. Der Text darf maximal 500 bytes lang sein und aus
                       nur einem Absatz bestehen (Vorgaben des Dienstleisters).
                       Achtung, manche Zeichen belegen mehrere Bytes.

  -o --outfile         Übersetzung zusätzlich in eine Datei schreiben. Wenn Zu-
                       sätzlich die Option -f angegeben ist, wird die in der
                       Konfigurationsdatei hinterlegte Ausgabedatei verwendet.
                       Ist keine Ausgabedatei definiert oder wird Option -f
                       nicht angegeben, wird die Standardausgabedatei
                       ~/aCMTT-translations.txt verwendet. Jede weitere Überset-
                       zung wird bei erneuter Angabe von -o (oder entsprechendem
                       Eintrag in der Konfigurationsdatei) an eine vorhandene
                       Datei angefügt.
  -o --outfile <Datei>  Wie vorstehend, jedoch mit expliziter Angabe einer Aus-
                       gabedatei. Evtl. in der Konfigurationsdatei diesbezüg-
                       liche enthaltene Vorgaben werden dabei auch bei Angabe
                       von -f übergangen.

  -p  --private-tm     Aktiviert die ausschließliche Nutung einer privaten
                       Übersetzungsbibliothek (Erfordert die gleichzeitige
                       Angabe von -k bzw. entsprechende Einträge in der Konfigu-
                       rationsdatei)

  -q  --quota          Zeigt das verbelibende Tageskontingent (Worte) in der
                       Ausgabe an falls -r nicht zugleich aktiviert ist. (Noch
                       nicht verfügbar in Version 0.18)

  -r --raw-display     Gibt ausschließlich die Übersetzung des Ausgangstextes
                       auf der Standardausgabe aus. (Bei erfolgreicher Überset-
                       zung wird bei Programmende stets der Status 0 zurückge-
                       geben, bei nicht erfolgreicher Übersetzung ist der
                       Exitstatus 1)

  -s --source-language <ll[-CC]>  Sprache des Quelltextes, entweder gemäß ISO
                       Standard oder RFC 3066 und 3166, mit BINDESTRICH als
                       Trennzeichen (Beispiele: fr-FR oder fr-BE oder fr)

  -t --target-language <ll[-CC]>  Zielsprache der Übersetzung, entweder gemäß
                       ISO Standard oder RFC 3066 und 3166 mit BINDESTRICH
                       als Trennzeichen (Beispiele: fr-FR oder fr-BE oder fr)

  -u --human-only      Nur von Menschen übersetzte Satzfragmente für die Über-
                       setzung nutzen, unterbindet die Verwendung maschinen-
                       generierter Übersetzungen bei der Abfrage.

  -v --verbose         Aktiviert die Debugausgabe auf der Konsole

  -w --write-settings  Schreibt alle zugleich auf der Befehlszeile angegebenen
                       Optionen und Argumente als Standardeinstellung in die
                       Standardkonfigurationsdatei.
  -w --write-settings  <Konfigurationsdatei> Schreibt alle zugleich auf der
                       Befehlszeile angegebenen Optionen und Argumente als Stan-
                       dardeinstellung in die angegebene Konfigurationsdatei.
                       Die Optionen -c -e -g -i -k -o -p -r -s -t -u können mit
                       und ohne ihre möglichen Argumente angegeben werden. Ein
                       Ändern einer in der Konfigurationsdatei hinterlegten Ein-
                       stellung ist durch erneute Angabe der betreffenden Option
                       mit oder ohne Argument zusammen mit -w möglich.
                       Wird als Argument zu den genannten Optionen % angegeben,
                       wird die jeweilige Einstellung in der Konfigurationsdatei
                       deaktiviert, aber nicht gelöscht. Durch erneuten Aufruf
                       der Option zusammen mit -w wird sie in der Konfigura-
                       tionsdatei wieder aktiviert. Durch Angabe von %% als
                       Argument zu den genannten Optionen wird die jeweilige
                       Einstellung zusammen mit ihren hinterlegten Argumenten
                       aus der Konfigurationsdatei getilgt.

  -x --exchange        Kehrt eine in der Konfigurationsdatei definierte Überset-
                       zungsrichtung um. Die als Ausgangssprache festgelegte
                       Sprache wird als Zielsprache der Übersetzung verwendet
                       und umgekehrt. Auch der Wechsel der Funktion nur einer
                       Sprache von Ziel- zu Ausgangssprache und umgekehrt ist so
                       theoretisch möglich, was die Ergänzung mit anderen Spra-
                       chen in wechselnder Übersetzungsrichtung gestattet.

  -y       <ip-address>  Mit dieser Option kann eine alternative IP Adresse für
  -y                   die Verbindungsprüfung angegeben werden. Standardmäßig,
                       ohne explizite Angabe einer anderen IP mit dieser Option
                       oder in der Konfigurationsdatei, wird 8.8.8.8 (google
                       primary dns server) verwendet.

  -z       <URL>       Mit dieser Option kann ein alternativer Dienstleister zur
  -z                   Ermittlung der eigenen öffentlichen IP zur Übermittlung
                       an den Übersetzungsdienstleister (bei Verwendung der Op-
                       tion -i) angegeben werden. Standardmäßig (ohne explizite
                       Angabe eines anderen Dienstes mit dieser Option oder in
                       der Konfigurationsdatei) wird ipify.org verwendet.

  Bei gleichzeitiger Verwendung der Option -f zur Nutzung einer persönlichen
  Standardkonfiguration können die Optionen -a -e -g -i -k -o -p -r -s -t -u
  genutzt werden, um einzelne Einstellungen aus der persönlichen Konfigura-
  tionsdatei temporär für diesen Aufruf abzuändern oder zu ergänzen, indem die
  betreffende Option sowie ggf. ihre Argumente zusätzlich zu -f angegeben
  werden. Die betreffende in der Datei definierte Standardeinstellung wird
  damit temporär für diesen Befehlsaufruf ignoriert.
  Die Optionen -a -e -g -i -k -o -p -r -s -t -u können durch Angabe des Argu-
  ments % dazu verwendet werden, für den jeweiligen Befehlsaufruf einzelne in
  einer mit -f geladenen Konfigurationsdatei definierte Standardeinstellungen
  temporär außer Kraft zu setzen.

  Die Reihenfolge der Optionen im Befehlsaufruf ist wahlfrei. Lang- und Kurz-
  formen können beliebig gemischt verwendet werden. Groß-/Kleinschreibung der
  Optionen beachten.

  Fragen, Anregungen und Fehlerberichte bitte an: Bitte geben Sie einen Text zum Übersetzen an. Bitte geben Sie zwei verschiedene Sprachen an. Die aCMTT Konfigurationsdatei $settingsfile_d wurde nicht gefunden. Fehler: Erlaubte Textlänge in Absatz $count_paragraphs überschritten.\nErlaubt sind $mymemory_max_bytes Bytes. (Manche Zeichen benötigen mehrere Bytes.) Fehlerhaftes Befehlszeilenargument. Geänderte Konfiguration wurde in “$settingsfile_w” gespeichert: Kein Internetzugang.\nBitte überprüfen Sie ob Ihr Internetzugang funktionstüchtig ist! Kein Netzwerk.\nBitte überprüfen Sie ob Sie Zugang zu Ihrem Netzwerk haben! Keine Verbindung zum Übersetzungsdienst $check_service_url möglich.\nBitte überprüfen Sie, ob der Dienst gestört ist. Netzwerkfehler.\nBitte überprüfen Sie Ihre Netzwerkkonfiguration! Neue Konfigurationsdatei “$settingsfile_w” wurde angelegt: Quelltext: Ungültiges Befehlszeilenargument (doppeltes %% ist nur für Optionen zusammen mit -w erlaubt.) Version Übersetzt von Übersetzung: Project-Id-Version: aCMTT 0.21
Report-Msgid-Bugs-To: forum.antiXlinux.com
PO-Revision-Date: 2021-05-26 08:22+0200
Last-Translator: 
Language-Team: antiX-contribs (transifex.com)
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
Plural-Forms: nplurals=2; plural=(n != 1);
   aCMTT antiX Community Multilanguage Translation Tool, Ver. $version

  Usage: aCMTT [Options] \"<Text line>\"

  (-Shortform  --Longform  <Arguments>  Description)  It is only allowed either
  the short or the long form of an option to be filed. Arguments <...>
  are valid for both forms.

  -a --allow-cookies   Gives permission to translation service provider to use 
                       cookies and session cookies. The stored cookie file will get deleted
                       by aCMTT automatically for privacy reasons if it is older than 1 day. 

  -c --disable-check-connection  This option disables the preceding check of
                       connection to translation service provider which is performed by
                       default in order to prevent from undeterminable long waiting
                       time for response form servers.

  -d --display-settings  Displays recent content of default configuration file
                       only and exits program.
  -d --display-settings <configfile>  Displays recent content of configuration
                       file referred to by  <configfile> only and exit program.

  -e --email-address   Activates for this request the transmission of an
                       email address deposed in default config file. For this the
                       option -f has to be filed also. The -e option overrides the
                       default transmission setting deposed in config file.
  -e --email-address <emailadresse>  Transmits on this request the email-
                       address filed by <emailadresse> to the translation service
                       provider (in order to get granted the extended translation
                       quota)

  -f --from-settings   Reads all settings found in default configuration file 
                       ~/.config/aCMTT/aCMTT-settings for this translation request.
  -f --from-settings <settingsfile>  reads all settings found in configuration
                        file referred to by “<settingsfile>” for this translation request
                       All settings read by -f option can get overruled or comple-
                       mented by filing additional options on command line, without
                       touching the config file itself.

  -g --gui             Activates graphical user interface (GUI) of the 
                       program. (Not present in Version 0.18 still)

  -h --help            Short usage information about aCMTT.

  -i --ip-address      Activates the supplemental transmission of recent
                       public IP address to the translation service provider. (This
                       (is recommended by the provider for high volume usage)

  -k --api-key         Activates transmission of an api key deposed in con-
                       figuration file, given the -f option was also filed.
                       The correlating transmission setting found in configuration
                       file will get overruled by this option.
  -k --api-key <api-key>  Transfers the api key specified by <api-key> to
                       translation service provider. This key is only needed to access
                       a private translation library and can get obtained -- if and when
                       reqired -- from translation provider visiting the URL
                       https://mymemory.translated.net/doc/keygen.php 

  -l --line \"Textzeile\"  Mandatory option to file the text to get translated.
                       The text is to be enclosed by double quotation marks and may
                       not contain any standard quotation marks itself. Instead the
                       signs “ resp. „ and ” are to be used. The text allowed to a lenth
                       of 500 bytes maximum and only one paragraph per request is
                       permitted (by policies of translation service provider).
                       Keep in mind some characters may use multiple bytes.

  -o --outfile         Write translation to a local file additionaly. If -f option
                       is given also, the translation will get appended to file target
                       as deposed in configuration file. If no file target was found or
                       option -f was omitted the default target file
                       ~/aCMTT-translations.txt will be used.
                       If the file doesn't exist, it will get created.
  -o --outfile <file>  As before, but filing an explicit target file used this time.
                       This overrules a setting potentially present in configuration file,
                       even if option -f was filed.

  -p  --private-tm     Activates the exclusive usage of a private translation
                       library (requires the concurrent declaration of -k option and/or
                       corresponding entries in configuration file
)

  -q  --quota          Displays the remaining quota (words) in output as long
                       option -r was not filed concurrently. (Not present still
                       in version 0.18)

  -r --raw-display     Returns to console translated text exclusively. No
                       other text will get displayed, in case of error output will
                       stay empty. (On success program will return status code
                       0 and in case translation was not successfully performed
                       status 1 will get returned.

  -s --source-language <ll[-CC]>  Language identifier of source language,
                       either in compliance with ISO standard or RFC 3066 and 3166,
                       seperated by a DASH. Examples: fr-FR or fr-BE
                       or fr)

  -t --target-language <ll[-CC]>  Language Identifier of target language for
                       translation, either in compliance with ISO standard or RFC 3066
                       and 3166 seperated by a DASH. (Examples: fr-FR or fr-BE or fr)

  -u --human-only      When activating this option only human translated frag-
                       ments will be used for translation. Suppresses the use of machine-
                       generated translations for this request.

  -v --verbose         Activates Debug output on console

  -w --write-settings  Writes all options and arguments concurrently filed
                       on command line to default configuration file. These settings
                       will be read henceforth from file by using option -f in a request.
  -w --write-settings  <configuration file>  Writes all options and arguments
                       concurrently filed on command line to the specified configuration
                       file. They will be read henceforth from file by using option -f in a
                       request. The options -c -e -g -i -k -o -p -r -s -t -u can get used for
                       this as well with as without their respective arguments. Replace-
                       ment of entries deposed in a config file can be done by re-filing
                       the option in question with or without arguments concurrently
                       with option -w.
                       When filing % as argument to an option deposed in configuration
                       file, the setting will be deactivated, but not removed from file.
                       Filing this option again concurrently to option -w it will get acti-
                       vated in configuration file again. Filing double %% as argument
                       to one of the before mentioned options will remove (erase) the
                       option setting along with its deposed corresponding arguments
                       from the configuration file.

  -x --exchange        Reverses direction of translation as defined in confi-
                       guration file. The language set as source will be used for target
                       and vice versa. Theoretically it is also possible this way to ex-
                       change the role of a single deposed language only from source
                        to target, which allows complementing a given base language
                        by other foreign languages in alternating translation direction.

  -y       <ip-address>  This option allows to set an alternative IP address used for
  -y                   internet connection check. Per default, without explicitly filing
                       a different IP address with this option, 8.8.8.8 (google primary
                       dns server) will be used for a single ping.

  -z       <URL>       Using this option allows to set an alternative service provider
  -z                   to detect your public ip address for transmission to translation
                       service provider (when filing -i option). Per default (without
                       explicitly setting another Service with this option) ipify.org will
                       be used.

  Concurrently to the usage of option -f to read settings of your personal
  default configuration, the options  -a -e -g -i -k -o -p -r -s -t -u can get filed
  on commandline in order to overrule, supplement or omit specific settings
  to something different for this translation request temporarily which will
  result in ignoring the corresponding setting found in file.
  Options -a -e -g -i -k -o -p -r -s -t -u can get used to suspend them temporarily by
  supplying % as their argument when reading default settings by concurrently
  filed -f option on a translation request. Options are case sensitive.

  Sort order of options in command line is arbitrary. You may mix long- and
  short forms in the line.

  Questions, Suggestions and Bug reports please to: Please file a text to tanslate. Please file two different languages. No aCMTT configuration file $settingsfile_d was not found. Error: Maximum length of text in paragraph $count_paragraphs exceeded.\nAllowed are $mymemory_max_bytes Bytes. (Some characters use multiple Bytes.) Incorrect command line argument. Changed configuration was saved to: “$settingsfile_w”: No internet connection found\nPlease check your internet connection! No Network.\nPlease check whether your device is connected to your network! No connection to translation service provider $check_service_url possible.\nPlease check, whether the service is malfunctioning or down. Network error.\nPlease check your network configuration! An new configuration file “$settingsfile_w” was created: Text source: Invalid command line argument (Double %% is only allowed for options in connection with -w .) Version Translated by Translation: 