#!/bin/bash
# antiX Community Multilanguage Translate Tool (aCMTT)
# commandline translation tool using mymemory.translated.net
# written for antiX Community 5/2021 by Robin.antiX

# 1.) Preliminaries
# -----------------

TEXTDOMAINDIR=/usr/share/locale							# hint: source strings in German language this time. Keep in mind when creating .pot file!
TEXTDOMAIN=antiX-Community-Translation-Tool.sh

version=0.20

# set flags and variables
# don't change settings within script directly, please use config file, GUI and/or command line options instead.

flag_debug=0											# 1 or 0 ; default: 0; set to 1 will display debug output on terminal.
#flag_show_quota=0							# ( 8)		# 1 or 0 ; not in use by now. Set to 1 will display remaining translation quota in words per day.
flag_show_string_only=0						# ( 9)		# 1 or 0 ; default: 0; set to 1 will display translated string only on standard output (e.g. for scripting); aCMTT always returns error status “0” for success, “1” for translation error of any kind. Flag debug set to 1 overules this setting.
flag_cookies=0								# ( 1)		# 1 or 0 ; default: 0; set to 1 will save cookies and session cookies received from translation service provider for a day to avoid false quota exceeded messages.
flag_transmit_ip=0							# ( 4)		# 1 or 0 ; default: 0; if set to 1 your recent ip address will be additionally transmitted to translation service provider (recommended by api documentation for high volume usage).
flag_transmit_email=0						# ( 2)		# 1 or 0 ; default: 0; default setting allows 1000 words per day. If set to 1 your email address will be transmitted to translation service provider to extend your quoata to 10000 words per day.
flag_transmit_key=0							# ( 5)		# 1 or 0 ; default: 0; if set to 1 your mymemory api key will get transferred to translation provider in order to make use of your personal translation memmory. Only needed and available if you have an account. If needed you can create your key at https://mymemory.translated.net/doc/keygen.php
flag_only_human=0							# (10)		# 1 or 0 ; default: 0; if set to 1 only human translated segments will get used. default setting will use both, human and machine translated segments.
flag_only_private_tm=0						# ( 7)		# 1 or 0 ; default: 0; if set to 1 only matches from your private translation memmory will be used. Function only available if you have an account.
flag_keep_tempfiles=0									# 1 or 0 ; default: 0; keep tempfiles for debug on exit instead of deleting (only as long flag_debug is also set to 1). Be carefully, each time you run this tool additional tempfiles will be creted in your temp folder.
flag_disable_chk_conn=0						# (11)		# 1 od 0 ; default: 0; if set to 1 checking for active internet connection by sending a single ping to this ip address is disabled.
flag_write_settings=0									# 1 or 0 ; default: 0; used by script to decide whether settings from commandline input are to be stored in config file
flag_read_settings=0									# 1 or 0 ; default: 0; used by script to decide whether to read settings from config file if present; if not, create new settings file.
flag_show_settings=0									# 1 or 0 ; default: 0; used by script to decide when to cat settings file to terminal
flag_exit_dirty=0										# 1 or 0 ; default: 0; used by script to decide when to exit after loops
flag_help_main=0										# 1 or 0 ; default: 0; used by script to decide when to display help after loops
flag_gui=0									# ( 3)		# 1 or 0 ; default: 0; decides whether GUI will get displayed.
flag_write_outfile=0						# ( 6)		# 1 or 0 ; default: 0; if set to 1 text strings in source and translation will be added to outfile.
flag_exchange_lang=0									# 1 or 0 ; default: 0; if set to 1 direction of predefined (in config file) translation direction is exchanged for this program call.

mymemory_email=""							# (12)		# valid email address for extended request quota. Set flag_transmit_email to 1 to activate transmission to tranlation provider.
mymemory_key=""								# (13)		# your key received from translated.net. Not needed as long no personal translation memmory is to be used.
ipreport_service="https://api.ipify.org"	# (18)		# service provider used for grabbing own public ip if flag_transmit_ip is set to 1.
check_alive_ip="8.8.8.8"					# (17)		# this is google primary dns server; please adjust to whatever you like for checking of internet connection being functional; used for ping.
check_service_url="translated.net"						# base addres of translation service for availability check by ping
mymemory_url_pre="https://api.mymemory.translated.net/get?q="	# chunks of translation provider address (refer to api manual)
mymemory_url_post="&of=php"								# allowed: json or php. Hint: json returns hex-coded spechial characters, wheras php returns plain utf-8 strings in a serialised array.)
mymemory_url_pair="&langpair="							# chunk of request to translation provider (refer to api manual)
mymemory_max_bytes=500									# max allowed byte count per request (refer to api manual: max 500 bytes allowed)
lang_source=""											# initialising variable; takes source language identifier RFC3066-ISO3166 or two letter ISO (example: fr-FR or fr-BE or fr) from user input
lang_target=""								# (15)		# initialising variable; takes target language identifier RFC3066-ISO3166 or two letter ISO (example: fr-FR or fr-BE or fr) from user input
string_source=""							# (16)		# initialising variable; takes source string from user input
split_source=()											# initialising array; takes source string split to individual paragraphs (refer to api manual: only single paragraphs allowed)
count_paragraphs=0										# initialising variable; takes number of split paragraphs to translate
count_response=0										# initialising variable; takes number of correct responses from provider server
mymemory_response=()									# initialising array; takes response strings from service provider for individul paragraphs
CHR_0x0A="$(printf '\nx')"; CHR_0x0A=${CHR_0x0A%x}		# create special seperator
base_configdir="$HOME/.config/aCMTT"					# set base configuration directory
base_cookiefile="$base_configdir"/provider-cookies		# needed to store cookies for 24h
settingsfile="$base_configdir"/aCMTT-settings			# settings file for aCMTT
outfile="$HOME/aCMTT-translations.txt"		# (14)		# default output file for aCMTT
tempfile_01="/tmp/aCMTT-$$01.tmp"						# tempfiles needed for error trapping from wget and ping
tempfile_02="/tmp/aCMTT-$$02.tmp"						
txt_date="$(($(date +%s)/86400))"						# date expressed in days from 1/1/1970

flag_cookies_n=""					# ( 1)    hint: all these variables with affix “_n” will take command line arguments while being a shaddow of variables of same name without affix.
flag_transmit_email_n=""			# ( 2)
flag_gui_n=""						# ( 3)
flag_transmit_ip_n=""				# ( 4)
flag_transmit_key_n=""				# ( 5)
flag_write_outfile_n=""				# ( 6)
flag_only_private_tm_n=""			# ( 7)
flag_show_string_only_n=""			# ( 9)
flag_only_human_n=""				# (10)
flag_disable_chk_conn_n=""			# (11)
mymemory_email_n=""					# (12)
mymemory_key_n=""					# (13)
outfile_n=""						# (14)
lang_source_n=""					# (15)
lang_target_n=""					# (16)
check_alive_ip_n=""					# (17)
ipreport_service_n=""				# (18)
#flag_show_quota_n=""				# ( 8)

[ ! -d $base_configdir ] && mkdir -p "$base_configdir"	# create config dir if not existent

# read commandline options and arguments
while [[ $# -gt 0 ]]; do
	opt_cline="$1"
	case $opt_cline in	
		-a|--allow-cookies)		flag_cookies_n=1						# ( 1)
								shift
								if [ "$1" == "%" ]; then
									flag_cookies_n=0
									shift
								elif [ "$1" == "%%" ]; then
									flag_cookies_n="%"
									shift
								fi;;
		-c|--disable-check-connection)	flag_disable_chk_conn_n=1				# (11)
								shift
								if [ "$1" == "%" ]; then
									flag_disable_chk_conn_n=0
									shift
								elif [ "$1" == "%%" ]; then
									flag_disable_chk_conn_n="%"
									shift
								fi;;		
		-d|--display-settings)	flag_show_settings=1
								settingsfile_d="$settingsfile"
								flag_exit_dirty=1
								shift
								if [ -n "$(echo "$1" | sed 's/^-\([ac-iklopr-z]\)\|^--\(allow-cookies\|disable-check-connection\|display-settings\|email-address\|from-settings\|gui\|help\|ip-address\|api-key\|line\|outfile\|private-tm\|raw-display\|source-language\|target-language\|human-only\|verbose\|write-settings\|exchange\)$//')" ]; then
									settingsfile_d="$1"
									shift
								fi;;
		-e|--email-address)		flag_transmit_email_n=1					# ( 2)
								shift
								if [ -n "$(echo "$1" | sed 's/^-\([ac-iklopr-z]\)\|^--\(allow-cookies\|disable-check-connection\|display-settings\|email-address\|from-settings\|gui\|help\|ip-address\|api-key\|line\|outfile\|private-tm\|raw-display\|source-language\|target-language\|human-only\|verbose\|write-settings\|exchange\)$//')" ]; then
									mymemory_email_n="$1"				# (12)
									if [ "$1" == "%" ]; then
										flag_transmit_email_n=0
										mymemory_email_n=""
									elif [ "$1" == "%%" ]; then
										flag_transmit_email_n="%"
										mymemory_email_n="%"
									fi
									shift
								fi;;
		-f|--from-settings)		flag_read_settings=1
								settingsfile_f="$settingsfile"
								shift
								if [ -n "$(echo "$1" | sed 's/^-\([ac-iklopr-z]\)\|^--\(allow-cookies\|disable-check-connection\|display-settings\|email-address\|from-settings\|gui\|help\|ip-address\|api-key\|line\|outfile\|private-tm\|raw-display\|source-language\|target-language\|human-only\|verbose\|write-settings\|exchange\)$//')" ]; then
									settingsfile_f="$1"
									shift
								fi;;
		-g|--gui)				flag_gui_n=1							# ( 3)
								shift
								if [ "$1" == "%" ]; then
									flag_gui_n=0
									shift
								elif [ "$1" == "%%" ]; then
									flag_gui_n="%"
									shift									
								fi;;
		-h|--help)				flag_help_main=1
								flag_exit_dirty=1
								shift;;
		-i|--ip-address)		flag_transmit_ip_n=1					# ( 4)
								shift
								if [ "$1" == "%" ]; then
									flag_transmit_ip_n=0
									shift
								elif [ "$1" == "%%" ]; then
									flag_transmit_ip_n="%"
									shift														
								fi;;
		-k|--api-key)			flag_transmit_key_n=1					# ( 5)
								shift
								if [ -n "$(echo "$1" | sed 's/^-\([ac-iklopr-z]\)\|^--\(allow-cookies\|disable-check-connection\|display-settings\|email-address\|from-settings\|gui\|help\|ip-address\|api-key\|line\|outfile\|private-tm\|raw-display\|source-language\|target-language\|human-only\|verbose\|write-settings\|exchange\)$//')" ]; then
									mymemory_key_n="$1"					# (13)
									if [ "$1" == "%" ]; then
										flag_transmit_key_n=0
										mymemory_key_n=""
									elif [ "$1" == "%%" ]; then
										flag_transmit_key_n="%"
										mymemory_key_n"%"
									fi
									shift
								fi;;
		-l|--line)				:;
								shift
								if [ -n "$(echo "$1" | sed 's/^-\([ac-iklopr-z]\)\|^--\(allow-cookies\|disable-check-connection\|display-settings\|email-address\|from-settings\|gui\|help\|ip-address\|api-key\|line\|outfile\|private-tm\|raw-display\|source-language\|target-language\|human-only\|verbose\|write-settings\exchange\)$//')" ]; then
									string_source="$1"
									shift
								fi;;
		-o|--outfile)			flag_write_outfile_n=1					# ( 6)
								shift
								if [ -n "$(echo "$1" | sed 's/^-\([ac-iklopr-z]\)\|^--\(allow-cookies\|disable-check-connection\|display-settings\|email-address\|from-settings\|gui\|help\|ip-address\|api-key\|line\|outfile\|private-tm\|raw-display\|source-language\|target-language\|human-only\|verbose\|write-settings\|exchange\)$//')" ]; then
									outfile_n="$1"						# (14)
									if [ "$1" == "%" ]; then
										flag_write_outfile_n=0
										outfile_n=""
									elif [ "$1" == "%%" ]; then
										flag_write_outfile_n="%"
										outfile_n"%"
									fi
									shift
								fi;;						
		-p|--private-tm)		flag_only_private_tm_n=1				# ( 7)
								shift
								if [ "$1" == "%" ]; then
									flag_only_private_tm_n=0
									shift
								elif [ "$1" == "%%" ]; then
									flag_only_private_tm_n="%"
									shift																							
								fi;;
#		-q|--quota)				flag_show_quota_n=1						# ( 8)
#								shift
#								if [ "$1" == "%" ]; then
#									flag_show_quota_n=0
#									shift
#								elif [ "$1" == "%%" ]; then
#									flag_show_quota_n="%"
#									shift																							
#								fi;;
		-r|--raw-display)		flag_show_string_only_n=1				# ( 9)
								shift
								if [ "$1" == "%" ]; then
									fflag_show_string_only_n=0
									shift
								elif [ "$1" == "%%" ]; then
									fflag_show_string_only_n="%"
									shift
								fi;;
		-s|--source-language)	:;
								shift
								if [ -n "$(echo "$1" | sed 's/^-\([ac-iklopr-z]\)\|^--\(allow-cookies\|disable-check-connection\|display-settings\|email-address\|from-settings\|gui\|help\|ip-address\|api-key\|line\|outfile\|private-tm\|raw-display\|source-language\|target-language\|human-only\|verbose\|write-settings\|exchange\)$//')" ]; then
									lang_source_n="$1"					# (15)
									if [ "$1" == "%" ] || [ "$1" == "%%" ]; then
										lang_source_n="%"
									fi
									shift
								fi;;
		-t|--target-language)	:;
								shift
								if [ -n "$(echo "$1" | sed 's/^-\([ac-iklopr-z]\)\|^--\(allow-cookies\|disable-check-connection\|display-settings\|email-address\|from-settings\|gui\|help\|ip-address\|api-key\|line\|outfile\|private-tm\|raw-display\|source-language\|target-language\|human-only\|verbose\|write-settings\|exchange\)$//')" ]; then
									lang_target_n="$1"					# (16)
									if [ "$1" == "%" ] || [ "$1" == "%%" ]; then
										lang_target_n="%"
									fi									
									shift
								fi;;
		-u|--human-only)		flag_only_human_n=1						# (10)
								shift
								if [ "$1" == "%" ]; then
									flag_only_human_n="0"
									shift
								elif [ "$1" == "%%" ]; then
									flag_only_human_n="%"
									shift
								fi;;
		-v|--verbose)			flag_debug=1
								shift;;												
		-w|--write-settings)	flag_write_settings=1
								settingsfile_w="$settingsfile"
								flag_exit_dirty=1
								shift
								if [ -n "$(echo "$1" | sed 's/^-\([ac-iklopr-z]\)\|^--\(allow-cookies\|disable-check-connection\|display-settings\|email-address\|from-settings\|gui\|help\|ip-address\|api-key\|line\|outfile\|private-tm\|raw-display\|source-language\|target-language\|human-only\|verbose\|write-settings\|exchange\)$//')" ]; then
									settingsfile_w="$1"
									shift
								fi;;
		-x|--exchange)			flag_exchange_lang=1
								shift;;							
		-y)						:;
								shift
								if [ -n "$(echo "$1" | sed 's/^-\([ac-iklopr-z]\)\|^--\(allow-cookies\|disable-check-connection\|display-settings\|email-address\|from-settings\|gui\|help\|ip-address\|api-key\|line\|outfile\|private-tm\|raw-display\|source-language\|target-language\|human-only\|verbose\|write-settings\|exchange\)$//')" ]; then
									check_alive_ip_n="$1"				# (17)
									if [ "$1" == "%" ] || [ "$1" == "%%" ]; then
										check_alive_ip_n="%"
									fi
									shift
								fi;;
		-z)						:;
								shift
								if [ -n "$(echo "$1" | sed 's/^-\([ac-iklopr-z]\)\|^--\(allow-cookies\|disable-check-connection\|display-settings\|email-address\|from-settings\|gui\|help\|ip-address\|api-key\|line\|outfile\|private-tm\|raw-display\|source-language\|target-language\|human-only\|verbose\|write-settings\|exchange\)$//')" ]; then
									ipreport_service_n="$1"				# (18)
									if [ "$1" == "%" ] || [ "$1" == "%%" ]; then
										ipreport_service_n="%"
									fi
									shift
								fi;;								
		*)						echo $"Fehlerhaftes Befehlszeilenargument."
								exit 2
								shift;;
								
	esac
done


# option -d display settings as stored in settings file
if [ $flag_show_settings == 1 ]; then
	if [ -e $settingsfile_d ]; then
		cat "$settingsfile_d"
	else
		echo -e \n$"Die aCMTT Konfigurationsdatei $settingsfile_d wurde nicht gefunden."
	fi
	exit 1
fi

# option -w write settings from commandline to settings file; "%" removes entry.
if [ $flag_write_settings == 1 ]; then
	if [ ! -e $settingsfile_w ]; then
		echo -e "# aCMTT ver. $version configuration file.
# ===================================
# Please use -w option to add or remove 
# settings to or from this file.
# You also may edit this file manually:
# One entry per line, key and value, unqouted
# both separated by equal sign.
">"$settingsfile_w"
		echo -e "\n"$"Neue Konfigurationsdatei “$settingsfile_w” wurde angelegt:" 
	else
		echo -e "\n"$"Geänderte Konfiguration wurde in “$settingsfile_w” gespeichert:" 
	fi
	if [ -n "$flag_cookies_n" ]; then if [ "$flag_cookies_n" == "%" ]; then sed -i '/^allow_cookies..*$/d' "$settingsfile_w"; else sed -i '/^allow_cookies..*$/d;$a allow_cookies='$flag_cookies_n'' "$settingsfile_w"; fi; fi											# ( 1)
	if [ -n "$flag_transmit_email_n" ]; then if [ "$flag_transmit_email_n" == "%" ]; then sed -i '/^transmit_email..*$/d' "$settingsfile_w"; else sed -i '/^transmit_email..*$/d;$a transmit_email='$flag_transmit_email_n'' "$settingsfile_w"; fi; fi					# ( 2)
	if [ -n "$flag_gui_n" ]; then if [ "$flag_gui_n" == "%" ]; then sed -i '/^display_gui..*$/d' "$settingsfile_w"; else sed -i '/^display_gui..*$/d;$a display_gui='$flag_gui_n'' "$settingsfile_w"; fi; fi															# ( 3)
	if [ -n "$flag_transmit_ip_n" ]; then if [ "$flag_transmit_ip_n" == "%" ]; then sed -i '/^transmit_ip..*$/d' "$settingsfile_w"; else sed -i '/^transmit_ip..*$/d;$a transmit_ip='$flag_transmit_ip_n'' "$settingsfile_w"; fi; fi									# ( 4)
	if [ -n "$flag_transmit_key_n" ]; then if [ "$flag_transmit_key_n" == "%" ]; then sed -i '/^transmit_key..*$/d' "$settingsfile_w"; else sed -i '/^transmit_key..*$/d;$a transmit_key='$flag_transmit_key_n'' "$settingsfile_w"; fi; fi								# ( 5)
	if [ -n "$flag_write_outfile_n" ]; then if [ "$flag_write_outfile_n" == "%" ]; then sed -i '/^write_outfile..*$/d' "$settingsfile_w"; else sed -i '/^write_outfile..*$/d;$a write_outfile='$flag_write_outfile_n'' "$settingsfile_w"; fi; fi														# ( 6)
	if [ -n "$flag_only_private_tm_n" ]; then if [ "$flag_only_private_tm_n" == "%" ]; then sed -i '/^only_private_tm..*$/d' "$settingsfile_w"; else sed -i '/^only_private_tm..*$/d;$a only_private_tm='$flag_only_private_tm_n'' "$settingsfile_w"; fi; fi				# ( 7)
	#if [ -n "$flag_show_quota_n" ]; then if [ "flag_show_quota_n" == "%" ]; then sed -i '/^show_quota..*$/d' "$settingsfile_w"; else sed -i '/^show_quota..*$/d;$a show_quota='$flag_show_quota_n'' "$settingsfile_w"; fi; fi											# ( 8)
	if [ -n "$flag_show_string_only_n" ]; then if [ "$flag_show_string_only_n" == "%" ]; then sed -i '/^show_string_only..*$/d' "$settingsfile_w"; else sed -i '/^show_string_only..*$/d;$a show_string_only='$flag_show_string_only_n'' "$settingsfile_w"; fi; fi		# ( 9)
	if [ -n "$flag_only_human_n" ]; then if [ "$flag_only_human_n" == "%" ]; then sed -i '/^only_human..*$/d' "$settingsfile_w"; else sed -i '/^only_human..*$/d;$a only_human='$flag_only_human_n'' "$settingsfile_w"; fi; fi											# (10)
	if [ -n "$flag_disable_chk_conn_n" ]; then if [ "$flag_disable_chk_conn_n" == "%" ]; then sed -i '/^check_connection..*$/d' "$settingsfile_w"; else sed -i '/^check_connection..*$/d;$a check_connection='$flag_disable_chk_conn_n'' "$settingsfile_w"; fi; fi		# (11)
	if [ -n "$mymemory_email_n" ]; then if [ "$mymemory_email_n" == "%" ]; then sed -i '/^email_address..*$/d' "$settingsfile_w"; else sed -i '/^email_address..*$/d;$a email_address='$mymemory_email_n'' "$settingsfile_w"; fi; fi									# (12)
	if [ -n "$mymemory_key_n" ]; then if [ "$mymemory_key_n" == "%" ]; then sed -i '/^api_key..*$/d' "$settingsfile_w"; else sed -i '/^api_key..*$/d;$a api_key='$mymemory_key_n'' "$settingsfile_w"; fi; fi															# (13)
	if [ -n "$outfile_n" ]; then if [ "$outfile_n" == "%" ]; then sed -i '/^outfile..*$/d' "$settingsfile_w"; else sed -i '/^outfile..*$/d;$a outfile='$outfile_n'' "$settingsfile_w"; fi; fi																			# (14)
	if [ -n "$lang_source_n" ]; then if [ "$lang_source_n" == "%" ]; then sed -i '/^default_lang_source..*$/d' "$settingsfile_w"; else sed -i '/^default_lang_source..*$/d;$a default_lang_source='$lang_source_n'' "$settingsfile_w"; fi; fi							# (15)
	if [ -n "$lang_target_n" ]; then if [ "$lang_target_n" == "%" ]; then sed -i '/^default_lang_target..*$/d' "$settingsfile_w"; else sed -i '/^default_lang_target..*$/d;$a default_lang_target='$lang_target_n'' "$settingsfile_w"; fi; fi							# (16)
	if [ -n "$check_alive_ip_n" ]; then if [ "$check_alive_ip_n" == "%" ]; then sed -i '/^check_alive_ip..*$/d' "$settingsfile_w"; else sed -i '/^check_alive_ip..*$/d;$a check_alive_ip='$check_alive_ip_n'' "$settingsfile_w"; fi; fi									# (17)
	if [ -n "$ipreport_service_n" ]; then if [ "$ipreport_service_n" == "%" ]; then sed -i '/^ipreport_service..*$/d' "$settingsfile_w"; else sed -i '/^ipreport_service..*$/d;$a ipreport_service='$ipreport_service_n'' "$settingsfile_w"; fi; fi						# (18)
	cat "$settingsfile_w"
	echo ""
	exit 1
fi

# option -f read settings from file, for execution
if [ $flag_read_settings == 1 ]; then
	# check for present settingsfile and read values
	if [ -e "$settingsfile" ]; then
		[ $flag_debug == 1 ] && echo -e "settings file for aCMTT found.\nReading from “$settingsfile”"
		while read -r item; do
			key="$(echo $item | cut -s -d '=' -f 1)"
			value="$(echo $item | cut -s -d '=' -f 2)"
			if [ -n "$value" ]; then
				case $key in 
					allow_cookies)			flag_cookies="$value";;					# ( 1)
					transmit_email)			flag_transmit_email="$value";;			# ( 2)
					display_gui)			flag_gui="$value";;						# ( 3)
					transmit_ip)			flag_transmit_ip="$value";;				# ( 4)
					transmit_key)			flag_transmit_key="$value";;			# ( 5)
					write_outfile)			flag_write_outfile="$value";;			# ( 6)
					only_private_tm)		flag_only_private_tm="$value";;			# ( 7)
					#show_quota)			flag_show_quota="$value";;				# ( 8)
					show_string_only)		flag_show_string_only="$value";;		# ( 9)
					only_human)				flag_only_human="$value";;				# (10)
					check_connection)		flag_disable_chk_conn="$value";;		# (11)
					email_address)			mymemory_email="$value";;				# (12)
					api_key)				mymemory_key="$value";;					# (13)
					outfile)				outfile="$value";;						# (14)
					default_lang_source)	if [ $flag_exchange_lang == 0 ]; then	# (15)
												lang_source="$value"
											else
												lang_target="$value"
											fi;;				
					default_lang_target)	if [ $flag_exchange_lang == 0 ]; then	# (16)
												lang_target="$value"
											else
												lang_source="$value"
											fi;;					
					check_alive_ip)			check_alive_ip="$value";;				# (17)					
					ipreport_service)		ipreport_service="$value";;				# (18)
				esac
			fi
		done <"$settingsfile"
	else
		[ $flag_debug == 1 ] && echo -e "No aCMTT settings file found.\nCreating “$settingsfile”"
	fi
fi

# check for overrides to default settings by recent command line options
flag_invalid_arg=0
if [ -n "$flag_cookies_n" ]; then if [ "$flag_cookies_n" == "%" ]; then flag_cookies=0; else flag_cookies="$flag_cookies_n"; fi; fi													# ( 1)
if [ -n "$flag_transmit_email_n" ]; then if [ "$flag_transmit_email_n" == "%" ]; then flag_transmit_email=0; else flag_transmit_email="$flag_transmit_email_n"; fi; fi				# ( 2)
if [ -n "$flag_gui_n" ]; then if [ "$flag_gui_n" == "%" ]; then flag_gui=0; else flag_gui="$flag_gui_n"; fi; fi																		# ( 3)
if [ -n "$flag_transmit_ip_n" ]; then if [ "$flag_transmit_ip_n" == "%" ]; then flag_transmit_ip=0; else flag_transmit_ip="$flag_transmit_ip_n"; fi; fi								# ( 4)
if [ -n "$flag_transmit_key_n" ]; then if [ "$flag_transmit_key_n" == "%" ]; then flag_transmit_key=0; else flag_transmit_key="$flag_transmit_key_n"; fi; fi						# ( 5)
if [ -n "$flag_write_outfile_n" ]; then if [ "$flag_write_outfile_n" == "%" ]; then flag_write_outfile=0; else flag_write_outfile="$flag_write_outfile_n"; fi; fi					# ( 6)
if [ -n "$flag_only_private_tm_n" ]; then if [ "$flag_only_private_tm_n" == "%" ]; then flag_only_private_tm=0; else flag_only_private_tm="$flag_only_private_tm_n"; fi; fi			# ( 7)
#if [ -n "$flag_show_quota_n" ]; then if [ "flag_show_quota_n" == "%" ]; then flag_show_quota_n=0; else flag_show_quota_n="flag_show_quota_n"; fi; fi								# ( 8)
if [ -n "$flag_show_string_only_n" ]; then if [ "$flag_show_string_only_n" == "%" ]; then flag_show_string_only=0; else flag_show_string_only="$flag_show_string_only_n"; fi; fi	# ( 9)
if [ -n "$flag_only_human_n" ]; then if [ "$flag_only_human_n" == "%" ]; then flag_only_human=0; else flag_only_human="$flag_only_human_n"; fi; fi								# (10)
if [ -n "$flag_disable_chk_conn_n" ]; then if [ "$flag_disable_chk_conn_n" == "%" ]; then flag_disable_chk_conn=0; else flag_disable_chk_conn="$flag_disable_chk_conn_n"; fi; fi	# (11)
if [ -n "$mymemory_email_n" ]; then if [ "$mymemory_email_n" == "%" ]; then flag_invalid_arg=1; else mymemory_email="$mymemory_email_n"; fi; fi										# (12)
if [ -n "$mymemory_key_n" ]; then if [ "$mymemory_key_n" == "%" ]; then flag_invalid_arg=1; else mymemory_key="$mymemory_key_n"; fi; fi												# (13)
if [ -n "$outfile_n" ]; then if [ "$outfile_n" == "%" ]; then flag_invalid_arg=1; else outfile="$outfile_n"; fi; fi																	# (14)
if [ -n "$lang_source_n" ]; then if [ "$lang_source_n" == "%" ]; then flag_invalid_arg=1; else lang_source="$lang_source_n"; fi; fi													# (15)
if [ -n "$lang_target_n" ]; then if [ "$lang_target_n" == "%" ]; then flag_invalid_arg=1; else lang_target="$lang_target_n"; fi; fi													# (16)
if [ -n "$check_alive_ip_n" ]; then if [ "$check_alive_ip_n" == "%" ]; then flag_invalid_arg=1; else check_alive_ip="$check_alive_ip_n"; fi; fi										# (17)
if [ -n "$ipreport_service_n" ]; then if [ "$ipreport_service_n" == "%" ]; then flag_invalid_arg=1; else ipreport_service="$ipreport_service_n"; fi; fi								# (18)
if [ $flag_invalid_arg = 1 ]; then
	echo -e $"Ungültiges Befehlszeilenargument (doppeltes %% ist nur für Optionen zusammen mit -w erlaubt.)"
	exit 1
fi


if [ $flag_debug == 1 ]; then
echo -e "(-a) flag_cookies:\t$flag_cookies"															# ( 1)
echo -e "(-c) flag_disable_chk_conn:\t$flag_disable_chk_conn"										# (11)
echo -e "(-d) flag_show_settings:\t$flag_show_settings"
echo -e "(-e) flag_transmit_email:\t$flag_transmit_email\n\tmymemory_email:\t$mymemory_email"			# ( 2)(12)
echo -e "(-f) flag_read_settings:\t$flag_read_settings\n\tsettingsfile:\t$settingsfile"
echo -e "(-g) flag_gui:\t$flag_gui"																	# ( 3)
echo -e "(-h) flag_help:\t$flag_help"
echo -e "(-i) flag_transmit_ip:\t$flag_transmit_ip"													# ( 4)
echo -e "(-k) flag_transmit_key:\t$flag_transmit_key\n\tmymemory_key:\t$mymemory_key"					# ( 5)(13)
echo -e "(-l) string_source:\t$string_source"
echo -e "(-o) flag_write_outfile:\t$flag_write_outfile\n\toutfile:\t$outfile"							# ( 6)(14)
echo -e "(-p) flag_only_private_tm:\t$flag_only_private_tm"											# ( 7)
echo -e "(-q) flag_show_quota:\t$flag_show_quota"													# ( 8)
echo -e "(-r) flag_show_string_only:\t$flag_show_string_only"										# ( 9)
echo -e "(-s) lang_source:\t$lang_source"															# (15)
echo -e "(-t) lang_target:\t$lang_target"															# (16)
echo -e "(-u) flag_only_human:\t$flag_only_human"													# (10)
echo -e "(-w) flag_write_settings:\t$flag_write_settings"
echo -e "(-x) flag_exchange_lang:\t$flag_exchange_lang"										
echo -e "(-y) check_alive_ip:\t$check_alive_ip"														# (17)
echo -e "(-z) ipreport_service:\t$ipreport_service"													# (18)
fi


# prepare program help display (set for default 80 cpl terminal)
function help_main {
echo ""
echo $"  aCMTT antiX Community Multilanguage Translation Tool, Ver. $version

  Programmaufruf: aCMTT [Optionen] \"<Textzeile>\"

  (-Kurzform  --Langform  <Argumente>  Beschreibung)  Es darf immer nur wahl-
  weise entweder die Kurz- oder die Langform angegeben werden. Argumente <...>
  gelten für beide Formen.

  -a --allow-cookies   Erlaubt dem Überstzungsdienstleister die Verwendung von 
                       Cookies und Sitzungscookies. Die Cookiedatei wird aus
                       Datenschutzgründen von aCMTT automatisch gelöscht, sobald
                       sie älter als 1 Tag ist.

  -c --disable-check-connection  Diese Option deaktiviert die vorherige Überprü-
                       fung der Verbindung zum Übersetzungsdienstleister. Damit
                       können lange Wartezeiten auf ausbleibende Serverantworten
                       vermieden werden.

  -d --display-settings  Zeigt den aktuellen Inhalt der Standardkonfigurations-
                       datei an und verläßt das Programm.
  -d --display-settings <Konfigurationsdatei>  Zeigt den aktuellen Inhalt der
                       mit <Konfigurationsdatei> angegebene Datei an und verläßt
                       das Programm.

  -e --email-address   Aktiviert für diese Abfrage die Übertragung einer in der
                       Standardkonfigurationsdatei hinterlegten Emailadresse.
                       Dazu muß die Option -f zusätzlich angegeben sein. Die in
                       der Konfigurationsdatei definierte Übertragungseinstell-
                       ung wird dabei übergangen.
  -e --email-address <emailadresse>  Übermittelt bei dieser Abfrage die mit
                       <emailadresse> angegebene Anschrift an den Übersetzungs-
                       dienstleister (Zur Nutzung des erhöhten Kontingents)

  -f --from-settings   Läd alle in der Standard-Konfigurationsdatei 
                       ~/.config/aCMTT/aCMTT-settings enthaltenen Einstellungen.
  -f --from-settings <settingsfile>  läd aus der mit “<settingsfile>” angegebe-
                       nen Konfigurationsdatei alle darin vorhandenen Einstel-
                       lungen. Alle mit -f eingelesenen Festlegungen können
                       durch Angabe weiterer Optionen ergänzt oder abgeändert
                       werden, ohne die Konfigurationsdatei dabei
                       zu beeinflussen.

  -g --gui             Aktiviert die graphische Benutzeroberfläche (GUI) des
                       Programms. (In Version 0.18 noch nicht verfügbar)

  -h --help            Kurzhilfe zum Programm

  -i --ip-address      Aktiviert die zusätzliche Übertragung der der aktuellen
                       öffentlichen IP Adresse an den Übersetzungsdienstleister
                       (Vom Dienstleister bei starker Nutzung empfohlen)

  -k --api-key         Aktiviert die Übermittelung des in einer Konfigurations-
                       datei hinterlegten Schlüssels, sofern zusätzlich die
                       Option -f angegeben wird. Die in der Konfigurationsdatei
                       definierte Übertragungseinstellung wird dabei übergangen.
  -k --api-key <api-key>  Übermittelt den mit <api-key> angegebenen Schlüssel.
                       Dieser Schlüssel wird nur für die Nutzung einer privaten
                       Übersetzungsbibliothek benötigt und kann bei Bedarf unter
                       https://mymemory.translated.net/doc/keygen.php angefor-
                       dert werden.

  -l --line \"Textzeile\"  Obligatorische Angabe der zu übersetzenden Textzeile.
                       Der Text muß in doppelte Anführungszeichen eingschlossen
                       werden und darf selbst keine solchen Anführungszeichen
                       enthalten. Anstelledessen können “ bzw. „ und ” verwendet
                       werden. Der Text darf maximal 500 bytes lang sein und aus
                       nur einem Absatz bestehen (Vorgaben des Dienstleisters).
                       Achtung, manche Zeichen belegen mehrere Bytes.

  -o --outfile         Übersetzung zusätzlich in eine Datei schreiben. Wenn Zu-
                       sätzlich die Option -f angegeben ist, wird die in der
                       Konfigurationsdatei hinterlegte Ausgabedatei verwendet.
                       Ist keine Ausgabedatei definiert oder wird Option -f
                       nicht angegeben, wird die Standardausgabedatei
                       ~/aCMTT-translations.txt verwendet. Jede weitere Überset-
                       zung wird bei erneuter Angabe von -o (oder entsprechendem
                       Eintrag in der Konfigurationsdatei) an eine vorhandene
                       Datei angefügt.
  -o --outfile <Datei>  Wie vorstehend, jedoch mit expliziter Angabe einer Aus-
                       gabedatei. Evtl. in der Konfigurationsdatei diesbezüg-
                       liche enthaltene Vorgaben werden dabei auch bei Angabe
                       von -f übergangen.

  -p  --private-tm     Aktiviert die ausschließliche Nutung einer privaten
                       Übersetzungsbibliothek (Erfordert die gleichzeitige
                       Angabe von -k bzw. entsprechende Einträge in der Konfigu-
                       rationsdatei)

  -q  --quota          Zeigt das verbelibende Tageskontingent (Worte) in der
                       Ausgabe an falls -r nicht zugleich aktiviert ist. (Noch
                       nicht verfügbar in Version 0.18)

  -r --raw-display     Gibt ausschließlich die Übersetzung des Ausgangstextes
                       auf der Standardausgabe aus. (Bei erfolgreicher Überset-
                       zung wird bei Programmende stets der Status 0 zurückge-
                       geben, bei nicht erfolgreicher Übersetzung ist der
                       Exitstatus 1)

  -s --source-language <ll[-CC]>  Sprache des Quelltextes, entweder gemäß ISO
                       Standard oder RFC 3066 und 3166, mit BINDESTRICH als
                       Trennzeichen (Beispiele: fr-FR oder fr-BE oder fr)

  -t --target-language <ll[-CC]>  Zielsprache der Übersetzung, entweder gemäß
                       ISO Standard oder RFC 3066 und 3166 mit BINDESTRICH
                       als Trennzeichen (Beispiele: fr-FR oder fr-BE oder fr)

  -u --human-only      Nur von Menschen übersetzte Satzfragmente für die Über-
                       setzung nutzen, unterbindet die Verwendung maschinen-
                       generierter Übersetzungen bei der Abfrage.

  -v --verbose         Aktiviert die Debugausgabe auf der Konsole

  -w --write-settings  Schreibt alle zugleich auf der Befehlszeile angegebenen
                       Optionen und Argumente als Standardeinstellung in die
                       Standardkonfigurationsdatei.
  -w --write-settings  <Konfigurationsdatei> Schreibt alle zugleich auf der
                       Befehlszeile angegebenen Optionen und Argumente als Stan-
                       dardeinstellung in die angegebene Konfigurationsdatei.
                       Die Optionen -c -e -g -i -k -o -p -r -s -t -u können mit
                       und ohne ihre möglichen Argumente angegeben werden. Ein
                       Ändern einer in der Konfigurationsdatei hinterlegten Ein-
                       stellung ist durch erneute Angabe der betreffenden Option
                       mit oder ohne Argument zusammen mit -w möglich.
                       Wird als Argument zu den genannten Optionen % angegeben,
                       wird die jeweilige Einstellung in der Konfigurationsdatei
                       deaktiviert, aber nicht gelöscht. Durch erneuten Aufruf
                       der Option zusammen mit -w wird sie in der Konfigura-
                       tionsdatei wieder aktiviert. Durch Angabe von %% als
                       Argument zu den genannten Optionen wird die jeweilige
                       Einstellung zusammen mit ihren hinterlegten Argumenten
                       aus der Konfigurationsdatei getilgt.

  -x --exchange        Kehrt eine in der Konfigurationsdatei definierte Überset-
                       zungsrichtung um. Die als Ausgangssprache festgelegte
                       Sprache wird als Zielsprache der Übersetzung verwendet
                       und umgekehrt. Auch der Wechsel der Funktion nur einer
                       Sprache von Ziel- zu Ausgangssprache und umgekehrt ist so
                       theoretisch möglich, was die Ergänzung mit anderen Spra-
                       chen in wechselnder Übersetzungsrichtung gestattet.

  -y       <ip-address>  Mit dieser Option kann eine alternative IP Adresse für
  -y                   die Verbindungsprüfung angegeben werden. Standardmäßig,
                       ohne explizite Angabe einer anderen IP mit dieser Option
                       oder in der Konfigurationsdatei, wird 8.8.8.8 (google
                       primary dns server) verwendet.

  -z       <URL>       Mit dieser Option kann ein alternativer Dienstleister zur
  -z                   Ermittlung der eigenen öffentlichen IP zur Übermittlung
                       an den Übersetzungsdienstleister (bei Verwendung der Op-
                       tion -i) angegeben werden. Standardmäßig (ohne explizite
                       Angabe eines anderen Dienstes mit dieser Option oder in
                       der Konfigurationsdatei) wird ipify.org verwendet.

  Bei gleichzeitiger Verwendung der Option -f zur Nutzung einer persönlichen
  Standardkonfiguration können die Optionen -a -e -g -i -k -o -p -r -s -t -u
  genutzt werden, um einzelne Einstellungen aus der persönlichen Konfigura-
  tionsdatei temporär für diesen Aufruf abzuändern oder zu ergänzen, indem die
  betreffende Option sowie ggf. ihre Argumente zusätzlich zu -f angegeben
  werden. Die betreffende in der Datei definierte Standardeinstellung wird
  damit temporär für diesen Befehlsaufruf ignoriert.
  Die Optionen -a -e -g -i -k -o -p -r -s -t -u können durch Angabe des Argu-
  ments % dazu verwendet werden, für den jeweiligen Befehlsaufruf einzelne in
  einer mit -f geladenen Konfigurationsdatei definierte Standardeinstellungen
  temporär außer Kraft zu setzen.

  Die Reihenfolge der Optionen im Befehlsaufruf ist wahlfrei. Lang- und Kurz-
  formen können beliebig gemischt verwendet werden. Groß-/Kleinschreibung der
  Optionen beachten.

  Fragen, Anregungen und Fehlerberichte bitte an:"
echo -e "\t<forum.antiXlinux.com>"
echo -e ""
echo -e "  Copyright 2021 the antiX comunity"			# entry needed to comply with EU legislation
echo -e ""										# This GPL text may not get translated. See gnu.org for details.
echo -e "  This program is free software: you can redistribute it and/or"
echo -e "  modify it under the terms of the GNU General Public License as"
echo -e "  published by the Free Software Foundation, either version 3 of"
echo -e "  the License, or (at your option) any later version."
echo ""
echo -e "  This program is distributed in the hope that it will be useful,"
echo -e "  but WITHOUT ANY WARRANTY; without even the implied warranty of"
echo -e "  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
echo -e "  GNU General Public License for more details."
echo ""
echo -e "  You should have received a copy of the GNU General Public"
echo -e "  License along with this program.  If not, see "
echo -e "  \t<http://www.gnu.org/licenses/>."
echo ""
return 0
}

# display main help text
if [ $flag_help_main == 1 ]; then
	help_main
	exit 1
fi


# overule flags in debug mode
if [ $flag_debug == 1 ]; then
#	flag_show_quota=1
	flag_show_string_only=0
else
	flag_keep_tempfiles=0
fi


# preparing cleanup on leaving
function cleanup {
if [ $flag_keep_tempfiles == 0 ]; then
	[ -e $tempfile_01 ] && rm -f $tempfile_01
	[ -e $tempfile_01 ] && rm -f $tempfile_02
fi
#[ $flag_show_string_only == 0 ] && echo "mischief managed!"			# uncommenting this line allows to check whether cleanup is executed on any exit condition.
return 0
}
trap 'cleanup && exit 1' SIGINT
>$tempfile_01
>$tempfile_02


if [ $flag_exit_dirty == 1 ]; then
	#cleanup
	exit 1
fi


# check whether user has selected identical languages (we will not check for translations from two-letter to four-letter language code of same base language)
if [ "$lang_source" == "$lang_target" ]; then
	[ $flag_show_string_only == 0 ] && echo $"Bitte geben Sie zwei verschiedene Sprachen an."
	exit 1
fi


# check whether user has forgotten to give us something to translate
if [ $(echo $string_source | wc -w) == 0 ]; then
	[ $flag_show_string_only == 0 ] && echo $"Bitte geben Sie einen Text zum Übersetzen an."
exit 1
fi


[ $flag_show_string_only == 0 ] && echo -e "aCMTT "$"Version"" $version\n"


# prepare transmisson of email address (only if transmit email option is activated)
if [ $flag_transmit_email == 1 ]; then
	# check whether this looks like a valid email address:
	if [ $(echo "$mymemory_email" | wc -m) -gt 254 ] || [ $(echo "$mymemory_email" | tr '@' ' ' | wc -w) -ne 2 ]; then
		[ $flag_debug == 1 ] && echo "invalid email address: $mymemory_email (1)"
		cleanup
		exit 1
	else
		# check for correct characters and length of accountname
		if [ -n "$(echo "$mymemory_email" | cut -d '@' -f 1 | sed 's/[a-z0-9'"'"'.!#$%&*+/=?^_‘{|}~-]\{1,63\}//')" ]; then 
			[ $flag_debug == 1 ] && echo "invalid email address: $mymemory_email (2)"
			cleanup
			exit 1
		else
			# check for correct structure and length of all parts of domainname
			domainname=$(echo "$mymemory_email" | cut -d '@' -f 2)
			num_parts_domainname=$(echo "$domainname" | tr '.' ' ' | wc -w)
			if [ $num_parts_domainname -gt 125 ]; then					# this is a theoretical value from specs, never seen...
				[ $flag_debug == 1 ] && echo "invalid email address: $mymemory_email (3)" 
				cleanup
				exit 1
			else
				u=0
				for u in `seq 1 1 $num_parts_domainname`; do
					part_domainname=$(echo "$domainname" | cut -d '.' -f $u)
					# check for correct characters in domainname
					if [ -n "$(echo "$part_domainname" | sed 's/^[a-zA-Z0-9][a-zA-Z0-9\-]\{0,61\}[a-zA-Z0-9]$//')" ]; then
						[ $flag_debug == 1 ] && echo "invalid email address: $mymemory_email (4)"
						cleanup
						exit 1
					fi;
				done
			fi
		fi
	fi
	# ok, let's assume this is an email address.
	emailaddress="&de=$mymemory_email"
	[ $flag_debug == 1 ] && echo -e "email address “$mymemory_email” will\nadditionally get transmitted to translation provider\n"
else
	emailaddress=""
	[ $flag_debug == 1 ] && echo -e "transmission of email address to translation provider is disabled\n"
fi

# prepare usage of cookies (only if cookie option is activated)
if [ $flag_cookies == 1 ]; then
	recent_cookiefile="$base_cookiefile"-"$txt_date"
	previous_cookiefile="$base_cookiefile"-"$(($txt_date-1))"
	if [ -e "$recent_cookiefile" ]; then
		cookiefile="$recent_cookiefile"
	elif [ -e $previous_cookiefile ]; then
		cookiefile="$previous_cookiefile"
	else
		rm -f "$base_cookiefile"*
		cookiefile="$recent_cookiefile"
>"$cookiefile"
	fi
	cookies="--load-cookies $cookiefile --save-cookies $cookiefile --keep-session-cookies"
	if [ $flag_debug == 1 ]; then
		echo "stored translation provider cookies:"
		cat "$cookiefile"
		echo -e "\nuse of cookies by translation provider is activated.\n"
	fi
else
	[ $flag_debug == 1 ] && echo -e "translation provider cookies disabled\n"
	rm -f "$base_cookiefile"*
	cookies=""
fi

# check first whether we have internet connection, otherwise we'll have to wait ~20-30 Seconds for DNS server response for translation service check. (only if flag check_connection is set)
if [ $flag_disable_chk_conn == 0 ]; then
	sudo -- ping -n -c 1 -w 5 $check_alive_ip &>>$tempfile_01
	i=$?
	if [ $i == 2 ]; then
		[ $flag_debug == 1 ] && cat $tempfile_01
		[ $flag_show_string_only == 0 ] && echo -e $"Kein Netzwerk.\nBitte überprüfen Sie ob Sie Zugang zu Ihrem Netzwerk haben!"
		cleanup
		exit 1
	elif [ $i == 1 ]; then
		[ $flag_debug == 1 ] && cat $tempfile_01
		[ $flag_show_string_only == 0 ] && echo -e $"Kein Internetzugang.\nBitte überprüfen Sie ob Ihr Internetzugang funktionstüchtig ist!"
		cleanup
		exit 1
	elif [ $i != 0 ]; then
		[ $flag_debug == 1 ] && cat $tempfile_01
		[ $flag_show_string_only == 0 ] && echo -e $"Netzwerkfehler.\nBitte überprüfen Sie Ihre Netzwerkkonfiguration!"
		cleanup
		exit 1
	fi

	# check whether translation service is available (only if flag check_connection is set)
	sudo -- ping -n -c 1 -w 5 $check_service_url &>>$tempfile_01
	if [ $? != 0 ]; then
		[ $flag_debug == 1 ] && cat $tempfile_01
		[ $flag_show_string_only == 0 ] && echo -e $"Keine Verbindung zum Übersetzungsdienst $check_service_url möglich.\nBitte überprüfen Sie, ob der Dienst gestört ist."
		cleanup
		exit 1
	fi
else
	[ $flag_debug == 1 ] && echo -e "Check for internet connection and whether translation server is down is deactivated."
fi

# get own public IP address
if [ $flag_transmit_ip == 1 ]; then
	# pub_ip="$(wget -O - -- $ipreport_service | tr -d ' ' 2>>$tempfile_01)"
	pub_ip="$(wget -O - -- $ipreport_service 2>>$tempfile_02 | tr -d ' ')"
	#pub_ip="$(wget -O - -- $ipreport_service | tr -d ' ')"
	[ $flag_debug == 1 ] && cat "$tempfile_02"
	# check whether we have got a valid IP
	if [ "$(echo "$pub_ip" | sed 's/\([0-9]\{1,3\}\.\)\{3,\}[0-9]\{1,3\}//')" == "" ]; then
		# structure looks like an IP, but let's see in detail:
		for u in `seq 1 1 4`; do
			if [ $(echo "$pub_ip" | cut -d'.' -f $u) -gt 255 ]; then
				# this is not a valid IP"
				[ $flag_debug == 1 ] && echo -e "\nip check service provider didn't return a valid\nIP for transmission to translation service provider"
				cleanup
				exit 1
			fi
		done
	else
		# this is not an IP at all
		[ $flag_debug == 1 ] && echo -e "ip check service provider didn't return a valid IP\nfor transmission to translation service provider"
		cleanup
		exit 1
	fi
	# compose commandline parameter from ip received
	ipaddress="&ip=$pub_ip"
	[ $flag_debug == 1 ] && echo -e "recent own public ip address “$pub_ip” will\nadditionally get transmitted to translation provider\n"
else
	ipaddress=""
	[ $flag_debug == 1 ] && echo -e "transmission of own public ip address to translation provider is disabled\n"
fi

# activate key transmission to translation provider
if [ $flag_transmit_key == 1 ]; then
	if [ -z $mymemory_key ]; then
		[ $flag_debug == 1 ] && echo -e "No private api key found\n"
		cleanup
		exit 1
	fi
	apikey="&key=$mymemory_key"
	[ $flag_debug == 1 ] && echo -e "your api key “$mymemory_key” will\nadditionally get transmitted to translation provider\n"
	#activate/deactivate exclusive use of private translation memory
	if [ $flag_only_private_tm == 1 ]; then
		privatetm="&onlyprivate=1"
	else
		privatetm="&onlyprivate=0"
	fi
else
	[ $flag_debug == 1 ] && echo -e "transmission of api key to translation provider is disabled\n"
	apikey=""
	privatetm=""
fi

# request only human translated segments to be used
if [ $flag_only_human == 1 ]; then
	humanonly="&mt=0"
	[ $flag_debug == 1 ] && echo -e "for translation only human translated segments will get used.\n"
else
	[ $flag_debug == 1 ] && echo -e "for translation will get used human translated as well as machine translated segments.\n"
	humanonly=""
fi


# 2.) getting user input for translation from GUI
# -----------------------------------------------
#lang_source="de-DE"
#lang_target="fr-FR"
#string_source="Die Apfelbäume stehen in voller Blüte. Wenn es keinen unerwarteten Kälteeinbruch gibt, kann es in diesem Jahr eine gute Ernte werden."


# 3.) process input
# --------------------------------------
# seperate paragraphs:
while read -d "$CHR_0x0A" item; do
	split_source+=("$item")
	[ $flag_debug == 1 ] && echo -e "item found:\t$item"
	# check string length: 
	[ $flag_debug == 1 ] && echo -e "Stringlength paragraph $count_paragraphs:\t$(echo $item | wc -c) bytes."
	if [ $(echo $item | wc -c) -ge $mymemory_max_bytes ]; then
		[ $flag_show_string_only == 0 ] && echo -e $"Fehler: Erlaubte Textlänge in Absatz $count_paragraphs überschritten.\nErlaubt sind $mymemory_max_bytes Bytes. (Manche Zeichen benötigen mehrere Bytes.)"
		cleanup
		exit 1
	fi
	let $((count_paragraphs++))
done <<<"$string_source"
[ $flag_debug == 1 ] && echo -e "number of paragraphs:\t$count_paragraphs\n"


# 4.) translate
# -------------
u=0
while [ $u -lt $count_paragraphs ]; do
	[ $flag_debug == 1 ] && echo -e "translating item ($u): ${split_source[u]}"
	if [ $(echo ${split_source[u]} | wc -w) -eq 0 ]; then
		mymemory_response+=("")					# don't ask provider for empty paragraphes to translate, but add them gracefully to output.
		let $((u++))
		count_response=$u
	else
		mymemory_response+=( "`wget $cookies -O - -- "$mymemory_url_pre${split_source[u]}$mymemory_url_pair$lang_source|$lang_target$mymemory_url_post$humanonly$apikey$privatetm$ipaddress$emailaddress" 2>>$tempfile_01`" )
		if [ $? -eq 0 ]; then
			let $((u++))
			count_response=$u
			[ $flag_debug == 1 ] && echo -e "response line ($(($u-1))) from mymemory:\n${mymemory_response[$((u-1))]}\n"
		else
			if [ $flag_debug == 1 ]; then
				echo -e "response line ($u) from mymemory:\n${mymemory_response[u]}\n"
				cat "$tempfile_01"
			fi
			break
		fi
	fi
	[ $u -ne $count_paragraphs ] && sleep 3	# don't flood servers of translation service provider
done


# 5.) check response
# ------------------
[ $flag_debug == 1 ] && echo -e "count response\t$count_response\n"
if [ $count_response -eq 0 ]; then
	[ $flag_show_string_only == 0 ] && echo -e "Keine Antwort vom Übersetzungsdienstleister.\nMögliche Gründe:\n\t-- Kontingent überschritten\n\t-- Zu viele Anfragen\n\t-- Nutzungsbedingungen evtl. geändert.\nBitte überprüfen Sie dies bei der Internetrepräsentanz des Übersetzungsdienstleisters."
	cleanup
	exit 1
fi
if [ $count_response -lt $count_paragraphs ]; then [ $flag_show_string_only == 0 ] && echo -e "Es konnten nicht alle Absätze übersetzt werden."; fi


# 6.) extract target string from response
# ---------------------------------------
u=0
while [ $u -lt $count_response ]; do
	byte_target="$(echo ${mymemory_response[u]} | cut -d'{' -f 3- | cut -d'}' -f 1 | sed 's/^.*";s:[0-9][0-9]*:"translation";s://' | cut -d':' -f 1)"
	[ $flag_debug == 1 ] && echo -e "\nAdding paragraph $u to output string; length of translation:\t$byte_target byte."
	sep=""
	if [ $((u+1)) != $count_response ]; then sep="$CHR_0x0A"; fi
	string_target="$string_target$(echo ${mymemory_response[u]} | cut -d'{' -f 3- | cut -d'}' -f 1 | sed 's/^.*";s:[0-9][0-9]*:"translation";s:[0-9][0-9]*:"//' | cut -b 1-$byte_target | sed 's/&#39;/’/g')$sep"
	let $((u++))
done


# 7.) display translated string
# -----------------------------
#[ $flag_show_string_only == 0 ] && echo -en "\n"
[ $flag_debug == 1 ] && echo -e "Translated output:\n--------------------------------------------------------------"
echo "$string_target"
[ $flag_debug == 1 ] && echo -e "--------------------------------------------------------------"
[ $flag_show_string_only == 0 ] && echo -e "\n"$"Übersetzt von"" $check_service_url"


# 8.) write output file if requested
# ----------------------------------
if [ $flag_write_outfile == 1 ]; then
	echo -e "\n("$"Quelltext:"" $lang_source)\n$string_source\n("$"Übersetzung:"" $lang_target)\n$string_target" >> "$outfile"
fi

cleanup
exit 0


# todo: check which user inputs need to get checked before handing them over to the next commands
# todo: add check whether output filename is valid
# todo: add gui
# todo: check whether multiple occurances of a single option is found
